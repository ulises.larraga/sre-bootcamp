module "random-pet" {
  source = "./random-stack"
}


#! issues: https://github.com/oracle/terraform-provider-oci/issues/1608
module "container-stack" {
  source = "./container-stack"
  container_count = 4
  image_name = "httpd"

  # providers = {
  #   docker = docker.docker
  # }
}

