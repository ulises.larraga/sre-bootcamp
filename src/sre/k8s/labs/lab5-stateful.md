## Lab5 - mongo db


Lets create a simple Mongo deployment using a service + statefulset


```bash
kubectl apply -f https://raw.githubusercontent.com/Deeptiman/go-cache-kubernetes/master/deploy_kubernetes/mongodb/mongodb-service.yaml -n labs

kubectl apply -f https://raw.githubusercontent.com/Deeptiman/go-cache-kubernetes/master/deploy_kubernetes/mongodb/mongodb-statefulset.yaml -n labs
```


1. describe the statefulset and store the output to lab5/describe-sts.logs
2. get the statefulset and store the output to lab5/get-sts.logs
3. create a port-forward in another terminal so you can reach mongodb in `localhost:27017`
4. ==ONLY IF== you have a mongodb client installed, configure a new connection to the port-forward.
5. Create a file called `lab5/configure.md` where you list all the required ==steps== you'll follow in order to configure an application that'll live in the same cluster and in the same namespace to use this mongodb.

