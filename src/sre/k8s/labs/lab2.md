
## 2. Lab2 - Services YAML

Create all 3 flavors of services and map them to your whoami (of lab2) pods.

- Create a Service 'whoami-cip' ClusterIP and map `8000:8000` ports
- Create a Service 'whoami-np' NodePort and map `8002:8000`.
- Create a service of type LoadBalancer 'whoami-lb' and map `8080:8000` ports.

store everything in lab3/ folder.



### Port forward ➡️

lets create a port-forward to the pod to acces it.

```bash
kubectl port-forward -n labs svc/whoami-cip 8000:80 #<- here
```

visit your browser at http://localhost:8000 a few times

To exit
++ctrl+c++

!!! Note
    you can use this to access databases, services, microservices, applications inside a cluster.
